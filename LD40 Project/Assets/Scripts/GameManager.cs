﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour 
{
    public static GameManager instance;

    [Header("UI Elements")]
    [SerializeField] private GameObject titlePanel;
    [SerializeField] private GameObject pausePanel;
    [SerializeField] private GameObject helpPanel;
    [SerializeField] private Text[] countdownText;

    [Header("Editor Assignments")]
    [SerializeField] private GameObject[] spawnLocations;
    [SerializeField] private GameObject[] itemSpawners;
    [SerializeField] private Transform playerSpawn;
    [SerializeField] private GameObject playerPrefab;
    [SerializeField] private NodeManager nodeManager;
    [SerializeField] private AudioSource deathAudioSource;
    [SerializeField] private AudioSource pickupAudioSource;
    [SerializeField] private AudioClip enemyDeath;
    [SerializeField] private AudioClip playerDeath;

    [Header("Public Variables")]
    public bool GameActive = false;
    public int TotalEnemies = 0;
    public int MaxEnemies = 10;
    public float spawnDelay = 2.0f;

    private bool gamePaused = false;
    private int activeSpawners = 0;

    // Use this for initialization
    private void Start () 
	{
        if (instance == null)
            instance = this;

        titlePanel.SetActive(true);
        pausePanel.SetActive(false);
        helpPanel.SetActive(false);
        //StartCoroutine(RestartGame());
	}
	
    private void Spawn()
    {
        GameActive = true;
        GameObject go = (GameObject)Instantiate(playerPrefab, playerSpawn.position, playerSpawn.rotation);

        PlayerStats.instance.DefaultValues();
        Camera.main.GetComponent<CameraFollow>().DefaultValues();
        nodeManager.StartGame();
        ActivateSpawner();
        ActivateItemSpawners();
        GameActive = true;
    }

	// Update is called once per frame
	private void Update () 
	{
        if (!GameActive && !gamePaused)
        {
            if(Input.GetButtonDown("Jump") || Input.GetButtonDown("Submit") && titlePanel.activeSelf)
            {
                titlePanel.SetActive(false);
                helpPanel.SetActive(true);
                return;
            }
        }

        if (!GameActive && !gamePaused)
        {
            if (Input.GetButtonDown("Jump") || Input.GetButtonDown("Submit") && helpPanel.activeSelf)
            {
                helpPanel.SetActive(false);
                StartCoroutine(RestartGame());
                return;
            }
        }

        if (GameActive && !gamePaused)
        {
            if(Input.GetButtonDown("Submit"))
            {
                Debug.Log("paused!");
                pausePanel.SetActive(true);
                gamePaused = true;
                Time.timeScale = 0.001f;
                return;
            }
        }

        if (gamePaused)
        {
            if (Input.GetButtonDown("Submit"))
            {
                Debug.Log("Unpaused!");
                pausePanel.SetActive(false);
                gamePaused = false;
                Time.timeScale = 1.0f;
                return;
            }
            else if (Input.GetButtonDown("Cancel"))
            {
                Time.timeScale = 1.0f;
                SceneManager.LoadScene(0);
            }
        }

        if (GameActive && activeSpawners < Mathf.FloorToInt(PlayerStats.instance.GetPlayerEgo()))
        {
            ActivateSpawner();
        }
	}

    private void ActivateSpawner()
    {
        spawnLocations[activeSpawners].SetActive(true);

        if (activeSpawners < spawnLocations.Length)
            activeSpawners++;
    }

    private void ActivateItemSpawners()
    {
        for (int i = 0; i < itemSpawners.Length; i++)
        {
            itemSpawners[i].SetActive(true);
        }
    }

    private void DeactivateSpawners()
    {
        for (int i = 0; i < itemSpawners.Length; i++)
        {
            itemSpawners[i].SetActive(false);
        }

        for (int i = 0; i < spawnLocations.Length; i++)
        {
            spawnLocations[i].SetActive(false);        
        }

        activeSpawners = 0;

        GameObject[] powerUps = GameObject.FindGameObjectsWithTag("Pickup");
        for (int i = 0; i < powerUps.Length; i++)
        {
            Destroy(powerUps[i]);
        }
    }

    public void PlayOnEnemyDeath()
    {
        if (deathAudioSource.isPlaying && deathAudioSource.clip == playerDeath)
            return;

        deathAudioSource.clip = enemyDeath;
        deathAudioSource.Play();
    }

    public void PlayOnPlayerDeath()
    {
        deathAudioSource.clip = playerDeath;
        deathAudioSource.Play();
    }

    public void PlayOnPickup()
    {
        pickupAudioSource.Play();
    }

    public void Restart()
    {
        GameActive = false;
        PlayOnPlayerDeath();
        nodeManager.EndGame();
        DeactivateSpawners();

        for (int i = 0; i < countdownText.Length; i++)
        {
            countdownText[i].rectTransform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
        }

        StartCoroutine(RestartGame());
    }

    IEnumerator RestartGame()
    {
        //Horribly innefficient to write like this, however it does what I need it to do for now. Will replace with an if loop when I'm not so tired.
        countdownText[2].gameObject.SetActive(true);
        float timer = Time.time + 1.0f;

        while(timer > Time.time)
        {
            countdownText[2].rectTransform.localScale =
                new Vector3
                (
                countdownText[2].rectTransform.localScale.x - 0.3f * Time.deltaTime,
                countdownText[2].rectTransform.localScale.y - 0.3f * Time.deltaTime,
                countdownText[2].rectTransform.localScale.z - 0.3f * Time.deltaTime
                );

            yield return null;
        }
        countdownText[2].gameObject.SetActive(false);
        countdownText[1].gameObject.SetActive(true);
        timer = Time.time + 1.0f;

        while(timer > Time.time)
        {
            countdownText[1].rectTransform.localScale =
	          new Vector3
	          (
	          countdownText[1].rectTransform.localScale.x - 0.3f * Time.deltaTime,
	          countdownText[1].rectTransform.localScale.y - 0.3f * Time.deltaTime,
	          countdownText[1].rectTransform.localScale.z - 0.3f * Time.deltaTime
	          );

            yield return null;
        }
        countdownText[1].gameObject.SetActive(false);
        countdownText[0].gameObject.SetActive(true);
        timer = Time.time + 1.0f;

        while (timer > Time.time)
        {
            countdownText[0].rectTransform.localScale =
              new Vector3
              (
              countdownText[0].rectTransform.localScale.x - 0.3f * Time.deltaTime,
              countdownText[0].rectTransform.localScale.y - 0.3f * Time.deltaTime,
              countdownText[0].rectTransform.localScale.z - 0.3f * Time.deltaTime
              );

            yield return null;
        }
        countdownText[0].gameObject.SetActive(false);
        Spawn();
    }
}