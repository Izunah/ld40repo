﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VectorFlowNode : MonoBehaviour 
{
    [SerializeField] private Transform target;
    [SerializeField] private Vector3 flowDirection = Vector3.forward;
    private float flowUpdateDelay = 0.33f;

    IEnumerator CalulcateFlowDirection()
    {
        while(GameManager.instance.GameActive)
        {
            flowDirection = (target.position - transform.position).normalized;
            yield return new WaitForSeconds(flowUpdateDelay);
        }
    }

    public Vector3 GetFlowDirection()
    {
        return flowDirection;
    }

    public void StartGame()
    {
        target = GameObject.FindGameObjectWithTag("Player").transform;
        StartCoroutine(CalulcateFlowDirection());
    }

    public void EndGame()
    {
        target = null;
        StopAllCoroutines();
    }
}