﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttack : MonoBehaviour 
{
    [SerializeField] private GameObject bulletPrefab;
    [SerializeField] private Transform shotSpawn;
    [SerializeField] private AudioSource audioSource;

    [SerializeField] private int damage;

    private bool inRange = false;

    private float baseShotDelay = 1.5f;
    private float shotDelay = 1.5f;
    private float shotDelayTimer;

    private void Start()
    {
        shotDelay = baseShotDelay - ((Mathf.Floor(PlayerStats.instance.GetPlayerEgo()) / 10.0f) - 0.1f);
    }

    private void Update()
    {
        if (!GameManager.instance.GameActive && inRange)
            inRange = false;

        if (!GameManager.instance.GameActive && shotDelay != baseShotDelay)
            shotDelay = baseShotDelay;

        if(inRange && shotDelayTimer < Time.time)
        {
            if (!GameManager.instance.GameActive)
                return;

            Attack();
            shotDelayTimer = Time.time + shotDelay;
        }
    }

    private void Attack()
    {
        audioSource.Play();
        GameObject go = (GameObject)Instantiate(bulletPrefab, shotSpawn.position, shotSpawn.rotation);
        DestroyOnCollision[] childGo = go.GetComponentsInChildren<DestroyOnCollision>();

        go.GetComponent<DestroyOnCollision>().damage = damage;
        if(childGo.Length > 0)
        {
            for (int i = 0; i < childGo.Length; i++)
            {
                childGo[i].damage = damage;
            }
        }

        Destroy(go, 1.0f);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Player"))
            inRange = true;
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.gameObject.CompareTag("Player"))
            inRange = false;
    }

    public bool InRange()
    {
        return inRange;
    }
}