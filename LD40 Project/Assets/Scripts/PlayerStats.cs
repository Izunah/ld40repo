﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerStats : MonoBehaviour 
{
    [SerializeField] private Slider healthSlider;
    [SerializeField] private Image healthFill;
    [SerializeField] private Slider egoSlider;

    [SerializeField] private Color fullColor;
    [SerializeField] private Color middleColor;
    [SerializeField] private Color emptyColor;

    public static PlayerStats instance;

    private float playerEgo = 1.0f;
    private int health = 10;
    private int maxHealth = 10;
    private int currentMaxHealth = 10;

    private int baseDamage = 2;
    private int currentDamage = 2;

    private GameObject player;
    private Components components;

	// Use this for initialization
	private void Start () 
	{
        if (instance == null)
            instance = this;
        else
            Destroy(this);
	}

    private void Update()
    {
        if (health <= 0 && GameManager.instance.GameActive)
            Die();
    }

    public void DefaultValues()
    {
        //Get the player and set everything we need
        player = GameObject.FindGameObjectWithTag("Player");
        components = player.GetComponent<Components>();

        healthSlider = components.healthSlider;
        healthFill = components.healthFill;
        egoSlider = components.egoSlider;

        //Set all default values
        playerEgo = 1.0f;
        currentDamage = baseDamage;
        currentMaxHealth = maxHealth;
        health = currentMaxHealth;

        healthSlider.maxValue = currentMaxHealth;
        healthSlider.value = health;

        egoSlider.maxValue = 10.0f;
        egoSlider.value = playerEgo;
    }

    private void Die()
    {
        Destroy(GameObject.FindGameObjectWithTag("Player"));
        GameManager.instance.Restart();
    }

    public void HealthPickup(int value)
    {
        int amountToMaxHp = currentMaxHealth - health;

        if (value <= amountToMaxHp)
        {
            health += value;
        }
        else if (value > amountToMaxHp)
        {
            health += amountToMaxHp;
            currentMaxHealth += value - amountToMaxHp;
            healthSlider.maxValue = currentMaxHealth;
            health = currentMaxHealth;
        }

        DamagePlayer(0); //Calling this will update the health slider
    }

    public void DamagePlayer(int value)
    {
        health -= value;
        health = Mathf.Clamp(health, 0, currentMaxHealth);

        healthSlider.value = health;

        float range = (float)health / (float)currentMaxHealth;
        if(range > 0.5)
        {
            healthFill.color = Color.Lerp(middleColor, fullColor, range / 2);
        }
        else
        {
            healthFill.color = Color.Lerp(emptyColor, middleColor, range * 2);
        }
    }

    public void AdjustEgo(float value)
    {
        playerEgo += value;
        playerEgo = Mathf.Clamp(playerEgo, 1.0f, 10.0f);
        egoSlider.value = playerEgo;
    }

    public float GetPlayerEgo()
    {
        return playerEgo;
    }

    public void AdjustDamage(int value)
    {
        currentDamage += value;
    }

    public int GetCurrentDamage()
    {
        return currentDamage;
    }
}