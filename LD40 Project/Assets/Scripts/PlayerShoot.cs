﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShoot : MonoBehaviour 
{
    [SerializeField] private GameObject bulletPrefab;
    [SerializeField] private Transform shotSpawn;

    [SerializeField] private AudioSource audioSource;

    private float startPitch;
    public float shotDelay = 1.25f;
    public float shotTimer = 0.0f;

    private void Start()
    {
        startPitch = audioSource.pitch;
    }

    // Update is called once per frame
    void Update () 
	{
		if(Input.GetAxis("RightTrigger") == 1 && (shotTimer < Time.time))
        {
            shotTimer = Time.time + 1/shotDelay;
            audioSource.pitch = Random.Range(startPitch - 0.1f, startPitch + 0.1f);
            audioSource.Play();
            //Replace with an object pool later on to make this less bad performance wise
            GameObject go = (GameObject)Instantiate(bulletPrefab, shotSpawn.position, shotSpawn.rotation);
            go.GetComponent<DestroyOnCollision>().damage = PlayerStats.instance.GetCurrentDamage();
            go.GetComponent<DestroyOnCollision>().source = "Player";
            Destroy(go, 1.0f);
        }
	}
}