﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PushCollider : MonoBehaviour 
{
    [SerializeField] private SphereCollider pushCollider;

    private void Start()
    {
        pushCollider.enabled = true;
        StartCoroutine(DisablePushCollider());
    }
    
    IEnumerator DisablePushCollider()
    {
        yield return new WaitForSeconds(0.25f);
        pushCollider.enabled = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Enemy"))
        {
            other.GetComponent<EnemyHealth>().AdjustHealth(9999999, "PushCollider");
        }
    }
}