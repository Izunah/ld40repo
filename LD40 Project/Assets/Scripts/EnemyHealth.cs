﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyHealth : MonoBehaviour 
{
    [SerializeField] private ParticleSystem deathEffect;
    [SerializeField] private Color fullColor;
    [SerializeField] private Color emptyColor;

    [SerializeField] private int maxHealth;
    private MeshRenderer mesh;

    private int currentMaxHealth;
    private int health;

    private void Start()
    {
        currentMaxHealth = Mathf.CeilToInt((float)maxHealth * PlayerStats.instance.GetPlayerEgo());
        health = currentMaxHealth;
        mesh = GetComponent<MeshRenderer>();
        mesh.material.color = fullColor;
    }

    private void Update()
    {
        if (!GameManager.instance.GameActive && currentMaxHealth != maxHealth)
        {
            currentMaxHealth = maxHealth;
        }
    }

    private void Die(string source)
    {
        if(source == "Player")
        {
            PlayerStats.instance.AdjustEgo(0.1f);
            GameManager.instance.TotalEnemies--;
            GameManager.instance.PlayOnEnemyDeath();
        }

        if (deathEffect != null)
        {
            deathEffect.transform.parent = null;
            deathEffect.Play();
            Destroy(deathEffect, deathEffect.main.duration);
        }

        Destroy(gameObject);
    }

    public void AdjustHealth(int value, string source)
    {
        health -= value;
        health = Mathf.Clamp(health, 0, currentMaxHealth);
        float lerpValue = (float)health / (float)currentMaxHealth;

        mesh.material.color = Color.Lerp(emptyColor, fullColor, lerpValue);
        if (health == 0)
        {
            Die(source);
        }
    }
}