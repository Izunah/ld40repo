﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour 
{
    [SerializeField] private Transform target;
    [SerializeField] private Vector3 offset;

    // Update is called once per frame
    void Update () 
	{
        if(target != null && GameManager.instance.GameActive)
            transform.position = target.position + offset;

        if (!GameManager.instance.GameActive)
        {
            target = GameObject.FindGameObjectWithTag("Finish").transform;
            transform.position = target.position + offset;
        }
	}

    public void DefaultValues()
    {
        target = GameObject.FindGameObjectWithTag("Player").transform;
        transform.position = target.position + offset;
    }
}