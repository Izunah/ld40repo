﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PlayerController : MonoBehaviour
{
    private Rigidbody rb;
    [SerializeField] private float moveSpeed = 10.0f;

    private float dashDelay = 1.0f;
    private float dashTimer = 0.0f;
    private float dashPower = 250.0f;

	// Use this for initialization
	void Start ()
    {
        rb = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        if (!GameManager.instance.GameActive)
            return;

        DoMovement();
        DoRotation();
	}

    private void DoMovement()
    {
        float horizontalMovement = Input.GetAxisRaw("Horizontal");
        float verticalMovement = Input.GetAxisRaw("Vertical");

        Vector3 movement = new Vector3(horizontalMovement, 0.0f, verticalMovement);
        rb.MovePosition(transform.position + (movement * moveSpeed * Time.fixedDeltaTime));
    }

    private void DoRotation()
    {
        float angleX = Input.GetAxisRaw("RightJoyX");
        float angleY = Input.GetAxisRaw("RightJoyY");
        float rotAngle = Mathf.Atan2(angleX, angleY) * Mathf.Rad2Deg;

        rb.MoveRotation(Quaternion.Euler(0.0f, rotAngle, 0.0f));
    }
}