﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOnCollision : MonoBehaviour 
{
    [SerializeField] private ParticleSystem deathEffect;
    public int damage = 0;
    public string source = "Null";

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.CompareTag("Enemy"))
        {
            EnemyHealth eh = collision.gameObject.GetComponent<EnemyHealth>();

            if (eh != null)
                eh.AdjustHealth(damage, "Player");
        }

        if(collision.gameObject.CompareTag("Player"))
        {
            PlayerStats.instance.DamagePlayer(damage);
        }

        if (deathEffect != null)
        {
            deathEffect.transform.parent = null;
            deathEffect.Play();
            Destroy(deathEffect.gameObject, deathEffect.main.duration);
        }

        Destroy(gameObject);
    }
}