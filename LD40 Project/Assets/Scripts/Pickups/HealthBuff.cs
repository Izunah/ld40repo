﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "LD40/Pickups/HealthBuff")]
public class HealthBuff : Pickup 
{
    public int powerEffect = 2;

    public override void DoEffect()
    {
        PlayerStats.instance.HealthPickup(powerEffect);
    }
}