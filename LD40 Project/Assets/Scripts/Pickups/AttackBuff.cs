﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "LD40/Pickups/AttackBuff")]
public class AttackBuff : Pickup 
{
    public int powerEffect = 2;

    public override void DoEffect()
    {
        PlayerStats.instance.AdjustDamage(powerEffect);
    }
}