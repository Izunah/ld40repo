﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Wave
{
    public GameObject ObjectToSpawn;
    public int Quantity;
    public float TimeToNextSpawn;
}

public class WaveList : MonoBehaviour
{
    public Wave[] units;
}