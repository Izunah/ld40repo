﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Components : MonoBehaviour 
{
    public Slider healthSlider;
    public Image healthFill;
    public Slider egoSlider;
}