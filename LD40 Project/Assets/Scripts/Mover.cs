﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour 
{
    public float moveSpeed = 10.0f;

    // Update is called once per frame
    void Update () 
	{
        transform.position = transform.position + (transform.forward * moveSpeed * Time.deltaTime);
	}
}
