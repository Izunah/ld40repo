﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour 
{
    [SerializeField] private GameObject objectToSpawn;
    [SerializeField] private float spawnInterval;

	// Use this for initialization
	private void OnEnable () 
	{
        StartCoroutine(SpawnObject());
	}

    IEnumerator SpawnObject()
    {
        yield return new WaitForSeconds(spawnInterval);

        while (GameManager.instance.GameActive)
        {

            if(GameManager.instance.TotalEnemies < GameManager.instance.MaxEnemies)
            {
                Instantiate(objectToSpawn, transform.position, transform.rotation);
                GameManager.instance.TotalEnemies++;
            }

            yield return new WaitForSeconds(spawnInterval);
        }
    }
}