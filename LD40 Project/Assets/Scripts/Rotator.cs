﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour 
{
    public Vector3 rotation;

	// Update is called once per frame
	private void Update () 
	{
        //transform.rotation = (transform.rotation * Quaternion.Euler(rotation));
        transform.Rotate(rotation * Time.deltaTime);
	}
}