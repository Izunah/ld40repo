﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour 
{
    [SerializeField] private EnemyAttack enemyAttack;
    [SerializeField] private Transform target;

    [SerializeField] private float moveSpeed;
    [SerializeField] private float turnSpeed;

    private float updateNodeDelay = 1.0f;
    private float updateNodeTimer = 0.0f;

    private Vector3 moveDirection = Vector3.forward;
    private Rigidbody rb;
    public VectorFlowNode node;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();

        if (target == null)
            target = GameObject.FindGameObjectWithTag("Player").transform;
    }

    // Update is called once per frame
    private void Update()
    {
        if(updateNodeTimer < Time.time)
        {
            node = NodeManager.instance.GetClosestNode(transform.position);
            updateNodeTimer = Time.time + updateNodeDelay;
        }

        if(GameManager.instance.GameActive && target == null)
        {
            target = GameObject.FindGameObjectWithTag("Player").transform;
        }
    }

    private void FixedUpdate()
    {
        rb.velocity = Vector3.zero;

        if (GameManager.instance.GameActive && node != null)
        {
            moveDirection = node.GetFlowDirection();
            RotateTowardsTarget();
            MoveToTarget();
        }
    }

    private void MoveToTarget()
    {
        if(!enemyAttack.InRange())
            rb.MovePosition(transform.position + (moveDirection.normalized * moveSpeed * Time.fixedDeltaTime));
    }

    private void RotateTowardsTarget()
    {
        if(enemyAttack.InRange() && target != null)
        {
            moveDirection = (target.position - transform.position).normalized;
        }

        Quaternion lookRot = Quaternion.LookRotation(moveDirection);
        Vector3 amountToRotate = Quaternion.Lerp(transform.rotation, lookRot, turnSpeed * Time.fixedDeltaTime).eulerAngles;
        rb.MoveRotation(Quaternion.Euler(0.0f, amountToRotate.y, 0.0f));
    }
}