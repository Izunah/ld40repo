﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NodeManager : MonoBehaviour 
{
    public static NodeManager instance;
    [SerializeField] private VectorFlowNode[] nodes;

    private void Start()
    {
        if (instance == null)
            instance = this;

        nodes = GameObject.FindObjectsOfType<VectorFlowNode>();
    }

    public VectorFlowNode GetClosestNode(Vector3 pos)
    {
        float distance = 0.0f;
        float closestDistance = Mathf.Infinity;
        VectorFlowNode closestNode = null;

        for (int i = 0; i < nodes.Length; i++)
        {
            distance = Vector3.Distance(nodes[i].transform.position, pos);

            if (distance < closestDistance)
            {
                closestDistance = distance;
                closestNode = nodes[i];
            }
        }

        return closestNode;
    }

    public void StartGame()
    {
        for (int i = 0; i < nodes.Length; i++)
        {
            nodes[i].StartGame();
        }
    }

    public void EndGame()
    {
        for (int i = 0; i < nodes.Length; i++)
        {
            nodes[i].EndGame();
        }
    }
}