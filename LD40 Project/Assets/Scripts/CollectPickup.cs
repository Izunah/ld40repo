﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectPickup : MonoBehaviour 
{
    [SerializeField] private Pickup pickup;
    [SerializeField] private float pickupLifeSpan;

    private void Start()
    {
        Destroy(gameObject, pickupLifeSpan);
    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.CompareTag("Player"))
        {
            pickup.DoEffect();
            GameManager.instance.PlayOnPickup();
            Destroy(gameObject);
        }
    }
}